/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he wishes. Each result get added to his COUNT score
- Whoever reaches 20 first wins the round

*/

var count1, count2, activePlayer, score, gameStarted;

score = [0, 0];

init();

document.querySelector('.btn-reset').innerHTML = '<i class="fas fa-plus-circle"></i> New Game';
document.querySelector('.btn-reset').addEventListener('click', init);

document.querySelector('.btn-skip').addEventListener('click', function() {
    nextPlayer();
});

function init() {
    count1 = 0;
    count2 = 0;
    activePlayer = 1;
    document.querySelector('#player-1').innerHTML = 'Player 1 <i class="fas fa-check-circle"></i>';
    document.querySelector('#dice-1').innerHTML = '&nbsp;';
    document.querySelector('#dice-2').innerHTML = '&nbsp;';
    document.querySelector('#count-1').innerHTML = '&nbsp;';
    document.querySelector('#count-2').innerHTML = '&nbsp;';
    document.querySelector('.btn-reset').innerHTML = '<i class="fas fa-sync"></i> Reset Round';
}

document.querySelector('.btn-roll').addEventListener('click', function() {
    var dice = Math.floor(Math.random() * 6) + 1;

    switch (activePlayer) {

        case 1:
            count1 += dice;
            document.querySelector('#dice-1').textContent = dice;
            document.querySelector('#count-1').textContent = "Total Count: " + count1;

            if (count1 >19) {
                score[0] += 1;
                document.querySelector('#player-1').textContent = "Player 1 WON";
                document.querySelector('#score-1').textContent = "WIN: " + score[0];
                init();
            } else {
                nextPlayer();
            }
            break;

        case 2:
            count2 += dice;
            document.querySelector('#dice-2').textContent = dice;
            document.querySelector('#count-2').textContent = "Total Count: " + count2;

            if (count2 >19) {
                score[1] += 1;
                document.querySelector('#player-2').textContent = "Player 2 WON";
                document.querySelector('#score-2').textContent = "WIN: " + score[1];
                init();
            } else {
                nextPlayer();
            }
            break;
    }
});

function nextPlayer() {
    document.querySelector('#player-' + activePlayer).innerHTML = 'Player ' + activePlayer;
    activePlayer === 1 ? activePlayer = 2 : activePlayer = 1;
    document.querySelector('#player-' + activePlayer).innerHTML = 'Player ' + activePlayer + '<i class="fas fa-check-circle"></i>';
}
